Slide 1 : 
	- Aspects matérielles des plateformes radio logicielle
	- Utilisation de plateforme avec des capacités de calcul importante 

Slide 2 : 
	- Introduction sur les cibles matérielles pour le calcul numérique
	- Figures Flexibilité (Reconfiguration le fonctionnalité de l'application à partir d'une desciption haut niveaux) vs performance en efficacité energétique
	- Deux grandes familles : CPU et ASIC
	- Famille qui offre un bon compromis entre les 2 : FPGA ... interconnecté d'élement logique
	- High perf (Virtex-7 offers 1500-4000 MAC at 400 MHz, reconfigurable)
	- Modification du circuit au niveau HW mais outils pour programmation "plus" haut niveau

Slide 3 : 
	- Famille de processeur hybrides 
	- Archi hétégogène : SW et HW
	- Presentation Zynq
	- Avantage de l'hétérogénéité ...
 	- ... mais outils de conception peuvent encore progresser pour adresser efficacement ce type de platforme. 

 Slide 4 : 
 	- On retrouve ces familles de composants dans les différnentes générations de plateforme SDR avec l'exemple des USRP
 	- 3 grandes fammilles d'USRP : USRP1 (2003/2004) low cost, USRP N210 (2008), USRP E310 performance élevé 
 	- Nombre d'élement logique 
 	- Comment ces éléments logiques peuvent être utilisé efficacement 

Slide 5 (Plan) : 

Slide 6 :  
	- SDR telle qu'introduite au départ 
	- Figure : 
		- Differentes fonctions des couches PHY voir MAC
		- Implementées sur cibles SW et HW 
		- SW décrit Haut-niveaux (C++, gnu radio...) sur ordinateur hotes 
		- HW decrit bas niveaux, archi et traitements fixes
	- trade-off : limite entre HW/SW (SDR ideal, non-idéal, compromis perf / flexibilité)

Slide 7 : 
	- 

Slide 8 : 
	- Hardware accelerator decrit en bas niveaux => modèle de prog non SDR
	- Notre proposition: Full FPGA-SDR avec description haut-niveaux d'une couche phy complete en ciblant du HW
	- HLS outil +/- mature 

Slide 9 : 
	- 

Slide 10 : 2 autres études sur un récepteurs WIFI-like 
	- Objectif : montrer la flexibilité de l'approche et qu'on peut generer rapidement différentes archi HW
	- A gauche : jouer sur la rapidité ds traitements en fonction des resources matérielles utilisées et donc augmenter le débit 
	- A droite : On ne joue pas sur le débit mais sur la conso en adaptant le nombre de bits sur lesquels sont représentées les données dans le récepteur. 
		- 3 archi à générer => fait rapidement avec l'approche proposée


Slide 11 : 
	- Travaux intitiés sur les archi à base de SoC
	- Voilà l'archi du Zynq et le design associé 
		- Gnu radio + traitement rapide efficace dans la partie FPGA 
		=> améliorer le flow de conception 
	- Voici un example de flow de conception qui pretent à utiliser conjointement SW et HW
		- Description haut-niveau de l'application
		- Outil fourni par constructeur permet de tout faire sauf le HW/SW partionning efficace 
	- Illustration d'une application exécutée avec differentes archi....
				

Slide 12 : 
	- Donc on s'est intéréssé à trouver le meilleur découpage HW/SW sur des Soc 
	- Point d'entrée : application tuilable 


Slide 13 : 
	- Tester cette méthode sur un égaliseur temps-fréquence 

	- Validation sur des cartes Zynq seule

Slide 14 : 
	- Pour aller vers des validations radio, création d'une plateforme

Slide 15 : 
	- PoC : Réseaux hétérogène mutiservice avec une structure de trame unifiée et un découpage T/F pour différent services
	- Déployable : Attack TEMPEST : 
		- intro problème
		- Système large bande, reconfigurable, gros traitement et portable. 







